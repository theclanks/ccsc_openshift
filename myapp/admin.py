from copy import deepcopy
from django.contrib import admin
from mezzanine.blog.admin import BlogPostAdmin
from mezzanine.blog.models import BlogPost
import urllib2, os, re
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.conf import settings

class MyBlogPostAdmin(BlogPostAdmin):
   
    def save_model(self, request, obj, form, change): 
        if obj.featured_image is None or not obj.featured_image:
            youtube_id = re.search('youtube.com\/embed\/(?P<id>[A-Z,a-z,_,0-9,-]+)[\",&]', obj.content)
            if not youtube_id is None: 
                nome = youtube_id.group('id')
                url = 'http://img.youtube.com/vi/'+nome+'/hqdefault.jpg'
                arquivo = File(self.handle_upload_url_file(url))
                destino = settings.MEDIA_ROOT+"/uploads/blog/youtube/"+nome+".jpg"
                os.rename(arquivo.name, destino)
                os.chmod(destino, 775)
                obj.featured_image = "uploads/blog/youtube/"+nome+".jpg"
	obj.save()

    def handle_upload_url_file(self, url):
        img_temp = NamedTemporaryFile()
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]
        img_temp.write(opener.open(url).read())
        img_temp.flush()
        return img_temp

admin.site.unregister(BlogPost)
admin.site.register(BlogPost, MyBlogPostAdmin)
