import os
from setuptools import setup, find_packages

setup(name='CCSC',
      version='1.0',
      description='Portal Comunidade Crista Sao Carlos',
      author='Luis Carlos Otte Junior',
      author_email='jrluiscarlos@yahoo.com.br',
      url='http://www.python.org/sigs/distutils-sig/',
      packages=find_packages(),
      include_package_data=True,
      install_requires=open('%s/cms/requirements.txt' % os.environ['OPENSHIFT_REPO_DIR']).readlines(),
     )
